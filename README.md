# README #
This is an Android application of the SmartController system which is used as a gateway between the Arduino device and the servers.

The code for the Arduino device is also in this repository in the folder "Arduino".

### Contributors ###
Oriol Fort, Albert Terrones, Héctor Arreola and Pau Madrero