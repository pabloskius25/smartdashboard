/*
  Serial Event example

 When new serial data arrives, this sketch adds it to a String.
 When a newline is received, the loop prints the string and
 clears it.

 A good test for this is to try it with a GPS receiver
 that sends out NMEA 0183 sentences.

 Created 9 May 2011
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/SerialEvent

 */
#define ProximityEchoPIN 9
#define ProximityTrigPIN 8
#define NumberOfSensors 4
#define NumberOfBasicSensors 2
#define NumberOfInterruptionSensors 1
#define NumberOfActuators 4

String inputString = "";         // a string to hold incoming data

//The sensors names must be diferent to "exception"

String sensorsList[] = {"Brightness_Arduino","WaterLevel_Arduino","Proximity_Arduino","SmokeDetector"};
String sensorsUnits[] = {       "lx"        ,        "m3"        ,        "cm"       ,    "on/off"   };
String basicSensors[] = {"Brightness_Arduino","WaterLevel_Arduino"};
String basicSensorsUnits[] = {   "lx"        ,        "m3"        };
int sensorsPins[] =     {         A0         ,         A1         };
String interruptionSensors[] = {"SmokeDetector"};
int interruptionPins[] =       {       2        };
String interruptionUnits[] =   {    "on/off"    };
String actuatorsList[] = {"Irrigation","Lighting","Roof","Heater"};
int actuatorsPins[] =    {     13     ,    12     , 11  ,   7    };
String sensorsString = "";
String basicSensorsString = "";
String interruptionSensorsString = "";
String actuatorsString = "";
boolean analogInterruption0Activated = false;

void setup() {
  // initialize serial:
  Serial1.begin(9600);
  Serial.begin(9600);
  
  pinMode(ProximityEchoPIN, OUTPUT); /*activación del pin 9 como salida: para el pulso ultrasónico*/
  pinMode(ProximityTrigPIN, INPUT); /*activación del pin 8 como entrada: tiempo del rebote del ultrasonido*/
  
  for(int i = 0; i < NumberOfActuators; ++i) {
    pinMode(actuatorsPins[i],OUTPUT);
  }  
  for(int i = 0; i < NumberOfSensors; ++i) {
    sensorsString += sensorsList[i] + ":" + sensorsUnits[i];
    if(i < NumberOfSensors-1)
      sensorsString += ",";
  }
  for(int i = 0; i < NumberOfBasicSensors; ++i) {
    basicSensorsString += basicSensors[i] + ":" + basicSensorsUnits[i];
    if(i < NumberOfBasicSensors-1)
      basicSensorsString += ",";
  }
  for(int i = 0; i < NumberOfInterruptionSensors; ++i) {
    pinMode(interruptionPins[i], INPUT);
    interruptionSensorsString += interruptionSensors[i] + ":" + interruptionUnits[i];
    if(i < NumberOfInterruptionSensors-1)
      interruptionSensorsString += ",";
  }
  for(int i = 0; i < NumberOfActuators; ++i) {
    actuatorsString += actuatorsList[i];
    if(i < NumberOfActuators-1)
      actuatorsString += ",";
  }

  attachInterrupt(digitalPinToInterrupt(interruptionPins[0]), digitalInterruption0, CHANGE); // change CHANGE to RISING if you only want to interrupt when the pin goes from LOW to HIGH
}

void loop() {
  if (Serial1.available() > 0) {
    // get the new byte:
    if (inChar == (char)3) {      
      if(inputString == "info") {
        sendInfo();
      }
      else if(inputString == "data") {
        receiveData();
      }
      else if(inputString == "fetch") {
        Serial1.print(getDataToSend() + "\n");
      }
      inputString = "";
    }
    else
      // add it to the inputString:
      inputString += inChar;
  }

  // START OF ANALOG INTERRUPTIONS

  boolean analogInterruption0Result = analogRead(sensorsPins[0]) < 300;

  if(analogInterruption0Result && !analogInterruption0Activated) {
    Serial1.print(getDataToSend() + ";interruptor:" + sensorsList[0] + "\n");
    analogInterruption0Activated = true;
  }
  else if (!analogInterruption0Result && analogInterruption0Activated) {
    //Serial1.print(getDataToSend() + ";interruptor:" + sensorsList[0] + "\n"); // Uncomment to send the interruption the first time the condition is false
    analogInterruption0Activated = false;
  }

  // END OF ANALOG INTERRUPTIONS

  delay(10);
  
}

String getDataToSend() {
  //Serial1.println("data" + getProximity() + ";" + getBasicSensorsValues());
  return "data" + getBasicSensorsValues() + ";" + getInterruptionSensorsValues();
}

// START OF DIGITAL INTERRUPTIONS

void digitalInterruption0() {
  Serial1.print(getDataToSend() + ";interruptor:" + interruptionSensors[0] + "\n");
}

// END OF DIGITAL INTERRUPTIONS

// START OF CUSTOM SENSORS

String getProximitySensorValue() {
  return "Proximity_Arduino:" + getProximity();
}

String getProximity() {
  digitalWrite(ProximityEchoPIN,LOW); /* Por cuestión de estabilización del sensor*/
  delayMicroseconds(2);
  digitalWrite(ProximityEchoPIN, HIGH); /* envío del pulso ultrasónico*/
  delayMicroseconds(10);
  long tiempo=pulseIn(ProximityTrigPIN, HIGH); /* Función para medir la longitud del pulso entrante. Mide el tiempo que transcurrido entre el envío
  del pulso ultrasónico y cuando el sensor recibe el rebote, es decir: desde que el pin 12 empieza a recibir el rebote, HIGH, hasta que
  deja de hacerlo, LOW, la longitud del pulso entrante*/
  int distancia= int(0.017*tiempo); /*fórmula para calcular la distancia obteniendo un valor entero*/
  /*Monitorización en centímetros por el monitor serial*/
  return "Proximity_Arduino:" + distancia;
}

// END OF CUSTOM SENSORS

// START OF AUTOMATIZED FUNCTIONS

void setNewValue(String actuator, String value) {
  for(int i = 0; i < NumberOfActuators; ++i) {
    if(actuatorsList[i] == actuator) {
      if(value == "true")
        digitalWrite(actuatorsPins[i],HIGH);
      else if(value == "false")
        digitalWrite(actuatorsPins[i],LOW);
      break;
    }
  }
}

String getBasicSensorsValues() {
  String result = "";
  for(int i = 0; i < NumberOfBasicSensors; ++i) {
    result += basicSensors[i] + ":" + analogRead(sensorsPins[i]);
    if(i < NumberOfBasicSensors-1)
      result += ";";
  }
  return result;
}

String getInterruptionSensorsValues() {
  String result = "";
  for(int i = 0; i < NumberOfInterruptionSensors; ++i) {
    result += interruptionSensors[i] + ":" + digitalRead(interruptionPins[i]);
    if(i < NumberOfInterruptionSensors-1)
      result += ";";
  }
  return result;
}

void receiveData() {
  for(int i = 0; i < NumberOfActuators; ++i) {
    String actuator = Serial1.readStringUntil(':');
    String value = Serial1.readStringUntil(';');
    setNewValue(actuator,value);
  }
}

void sendInfo() {
  //Serial1.println("info" + sensorsString + ";" + actuatorsString);
  Serial1.print("info" + basicSensorsString + "," + interruptionSensorsString + ";" + actuatorsString + "\n");
}

// END OF AUTOMATIZED FUNCTIONS


