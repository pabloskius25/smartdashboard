#include "DHT.h"

#define ProximityEchoPIN 9
#define ProximityTrigPIN 8
#define TemperatureHumidityPIN 7
#define NumberOfSensors 6
#define NumberOfBasicSensors 3
#define NumberOfInterruptionSensors 0
#define NumberOfActuators 1

#define DHTTYPE DHT11 
DHT dht(TemperatureHumidityPIN, DHTTYPE);

String inputString = "";         // a string to hold incoming data

//The sensors names must be diferent to "exception"

String sensorsList[] = {"Temperature Arduino","Humidity Arduino","Accelerometer X","Accelerometer Y","Accelerometer Z","Proximity Arduino"};
String sensorsUnits[] = {        "ºC"        ,        "%"       ,       "º"       ,       "º"       ,       "º"       ,        "cm"       };
String basicSensors[] = {"Accelerometer X","Accelerometer Y","Accelerometer Z"};
String basicSensorsUnits[] = {    "º"     ,       "º"       ,       "º"       };
int sensorsPins[] =     {         A0      ,       A1        ,       A2        };
String interruptionSensors[] = {};
int interruptionPins[] =       {};
String interruptionUnits[] =   {};
String actuatorsList[] = {"LED"};
int actuatorsPins[] =    {  13 };
String sensorsString = "";
String basicSensorsString = "";
String interruptionSensorsString = "";
String actuatorsString = "";
boolean analogInterruption0Activated = false;

void setup() {
  // initialize serial:
  Serial1.begin(9600);
  Serial.begin(9600);

  dht.begin();
  
  pinMode(ProximityEchoPIN, OUTPUT);
  pinMode(ProximityTrigPIN, INPUT);
  
  for(int i = 0; i < NumberOfSensors; ++i) {
    sensorsString += sensorsList[i] + ":" + sensorsUnits[i];
    if(i < NumberOfSensors-1)
      sensorsString += ",";
  }
  for(int i = 0; i < NumberOfBasicSensors; ++i) {
    basicSensorsString += basicSensors[i] + ":" + basicSensorsUnits[i];
    if(i < NumberOfBasicSensors-1)
      basicSensorsString += ",";
  }
  for(int i = 0; i < NumberOfInterruptionSensors; ++i) {
    pinMode(interruptionPins[i], INPUT);
    interruptionSensorsString += interruptionSensors[i] + ":" + interruptionUnits[i];
    if(i < NumberOfInterruptionSensors-1)
      interruptionSensorsString += ",";
  }
  for(int i = 0; i < NumberOfActuators; ++i) {
    pinMode(actuatorsPins[i],OUTPUT);
    actuatorsString += actuatorsList[i];
    if(i < NumberOfActuators-1)
      actuatorsString += ",";
  }

  //attachInterrupt(digitalPinToInterrupt(interruptionPins[0]), digitalInterruption0, CHANGE); // change CHANGE to RISING if you only want to interrupt when the pin goes from LOW to HIGH
}

void loop() {
  if (Serial1.available() > 0) {
    // get the new byte:
    char inChar = (char)Serial1.read();
    if (inChar == (char)3) {    
      if(inputString == "info") {
        sendInfo();
      }
      else if(inputString == "data") {
        receiveData();
      }
      else if(inputString == "fetch") {
        printData();
      }
      inputString = "";
    }
    else
      // add it to the inputString:
      inputString += inChar;
  }

  // START OF ANALOG INTERRUPTIONS

  /*boolean analogInterruption0Result = analogRead(sensorsPins[0]) < 300;

  if(analogInterruption0Result && !analogInterruption0Activated) {
    Serial1.print(getDataToSend() + ";interruptor:" + sensorsList[0] + "\n");
    analogInterruption0Activated = true;
  }
  else if (!analogInterruption0Result && analogInterruption0Activated) {
    //Serial1.print(getDataToSend() + ";interruptor:" + sensorsList[0] + "\n"); // Uncomment to send the interruption the first time the condition is false
    analogInterruption0Activated = false;
  }*/

  // END OF ANALOG INTERRUPTIONS

  delay(10);
  
}

void printData() {
  Serial1.print("data");
  printBasicSensorsValues();
  Serial1.print(";");
  printProximitySensor();
  Serial1.print(";");
  printTemperatureSensor();
  Serial1.print(";");
  printHumiditySensor();
  Serial1.print("\n");
}

// START OF DIGITAL INTERRUPTIONS

void digitalInterruption0() {
  printData();
  Serial1.print(";interruptor:" + interruptionSensors[0] + "\n");
}

// END OF DIGITAL INTERRUPTIONS

// START OF CUSTOM SENSORS

void printProximitySensor() {
  Serial1.print(sensorsList[5] + ":");
  Serial1.print(getProximity());
}

int getProximity() {
  digitalWrite(ProximityEchoPIN,LOW);
  delayMicroseconds(2);
  digitalWrite(ProximityEchoPIN, HIGH);
  delayMicroseconds(10);
  long timeTaken = pulseIn(ProximityTrigPIN, HIGH);
  int distance = int(0.017*timeTaken);
  return distance;
}

void printTemperatureSensor() {
  int t = dht.readTemperature();
  Serial1.print(sensorsList[0] + ":");
  Serial1.print(t);
}

void printHumiditySensor() {
  int h = dht.readHumidity();
  Serial1.print(sensorsList[1] + ":");
  Serial1.print(h);
}

// END OF CUSTOM SENSORS

// START OF AUTOMATIZED FUNCTIONS

void setNewValue(String actuator, String value) {
  for(int i = 0; i < NumberOfActuators; ++i) {
    if(actuatorsList[i] == actuator) {
      if(value == "true")
        digitalWrite(actuatorsPins[i],HIGH);
      else if(value == "false")
        digitalWrite(actuatorsPins[i],LOW);
      break;
    }
  }
}

String getBasicSensorsValues() {
  String result = "";
  for(int i = 0; i < NumberOfBasicSensors; ++i) {
    result += basicSensors[i] + ":" + analogRead(sensorsPins[i]);
    if(i < NumberOfBasicSensors-1)
      result += ";";
  }
  return result;
}

void printBasicSensorsValues() {
  for(int i = 0; i < NumberOfBasicSensors; ++i) {
    Serial1.print(basicSensors[i] + ":");
    Serial1.print(analogRead(sensorsPins[i]));
    if(i < NumberOfBasicSensors-1)
      Serial1.print(";");
  }
}

String getInterruptionSensorsValues() {
  String result = "";
  for(int i = 0; i < NumberOfInterruptionSensors; ++i) {
    result += interruptionSensors[i] + ":" + digitalRead(interruptionPins[i]);
    if(i < NumberOfInterruptionSensors-1)
      result += ";";
  }
  return result;
}

void receiveData() {
  for(int i = 0; i < NumberOfActuators; ++i) {
    String actuator = Serial1.readStringUntil(':');
    String value = Serial1.readStringUntil(';');
    setNewValue(actuator,value);
  }
}

void sendInfo() {
  Serial1.println("info" + sensorsString + ";" + actuatorsString);
}

// END OF AUTOMATIZED FUNCTIONS


