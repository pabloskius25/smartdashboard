package fr.ece.edu.eceproject.gardendashboard.model;

/**
 * Created by Pau on 21/02/2016.
 */
public class MyDevice {

    private String sensors;
    private String actuators;

    public MyDevice(String sensors, String actuators) {
        this.sensors = sensors;
        this.actuators = actuators;
    }

    public String getActuators() {
        return actuators;
    }

    public void setActuators(String actuators) {
        this.actuators = actuators;
    }

    public String getSensors() {
        return sensors;
    }

    public void setSensors(String sensors) {
        this.sensors = sensors;
    }

}
