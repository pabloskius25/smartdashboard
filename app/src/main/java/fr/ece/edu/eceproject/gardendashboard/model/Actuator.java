package fr.ece.edu.eceproject.gardendashboard.model;

/**
 * Created by Pau on 04/03/2016.
 */
public class Actuator {

    private String name;
    private boolean isActivated;

    public Actuator(String name, boolean isActivated) {
        this.name = name;
        this.isActivated = isActivated;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setIsActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
