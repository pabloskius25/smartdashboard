package fr.ece.edu.eceproject.gardendashboard.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import fr.ece.edu.eceproject.gardendashboard.fragments.SettingsFragment;

/**
 * Created by Pau on 21/02/2016.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

}
