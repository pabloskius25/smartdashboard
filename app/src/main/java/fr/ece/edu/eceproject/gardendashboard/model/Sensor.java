package fr.ece.edu.eceproject.gardendashboard.model;

/**
 * Created by Pau on 04/03/2016.
 */
public class Sensor {

    private String name;
    private float value;
    private String units;

    public Sensor(String name, float value, String units) {
        this.name = name;
        this.value = value;
        this.units = units;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
