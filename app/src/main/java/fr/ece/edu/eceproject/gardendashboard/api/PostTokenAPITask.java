package fr.ece.edu.eceproject.gardendashboard.api;

import android.bluetooth.BluetoothClass;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import fr.ece.edu.eceproject.gardendashboard.activities.MainActivity;
import fr.ece.edu.eceproject.gardendashboard.model.MyDevice;


/**
 * Created by Pau on 21/02/2016.
 */
public class PostTokenAPITask extends AsyncTask<Void,Void,Void> {

    private JSONObject parameters;
    private MainActivity context;
    private SharedPreferences prefs;
    private String TAG = PostTokenAPITask.class.getSimpleName();

    public PostTokenAPITask(MainActivity context, String token, MyDevice device, SharedPreferences prefs) {
        this.prefs = prefs;
        this.context = context;
        parameters = new JSONObject();
        Random rnd = new Random();
        try {
            String deviceName = this.prefs.getString("deviceName", "");
            if(deviceName.equals("")) {
                deviceName = "Device" + rnd.nextInt(9999);
                prefs.edit().putString("deviceName", deviceName).apply();
            }
            parameters.put("token", token);
            parameters.put("fullname", deviceName);
            parameters.put("temporarySensors", device.getSensors());
            parameters.put("temporaryActuators", device.getActuators());
            String idDevice = this.prefs.getString("idDevice", "");
            if(!idDevice.equals(""))
                parameters.put("idDevice", idDevice);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;

        try {
            Log.d(TAG, prefs.getString("servAddrPref", "http://eceproject.byte.cat/api/") + "devices");
            URL url = new URL(prefs.getString("servAddrPref","http://eceproject.byte.cat/api/") + "devices");

            Log.d(TAG, parameters.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(parameters.toString());
            out.close();

//            Log.d("gardenDashboard", String.valueOf(urlConnection.getResponseCode()));
//
//            String f = "";
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
//            String line;
//            while((line = in.readLine()) != null) {
//                f += line;
//            }
//            in.close();
//
//            Log.d("Error Response: ", f);

            //Get Response
            String resultJsonString = "";
            InputStream is = urlConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line = rd.readLine()) != null) {
                //Log.d("gardenDashboard", "Line received: " + line);
                resultJsonString += line;
            }
            rd.close();

            Log.d(TAG, "JSON received: " + resultJsonString);

            parseJsonString(resultJsonString);

        } catch (Exception e) {
            Log.e(TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    private void parseJsonString(String jsonString) throws JSONException {
        JSONObject resultObj = new JSONObject(jsonString);
        String idDevice = resultObj.getString("id");
        prefs.edit().putString("idDevice", idDevice).apply();
        Log.d(TAG,"ID of the device received: " + idDevice);
        context.updateServerState(true);
        context.initializeDataUpdatesTimer();
    }
}
