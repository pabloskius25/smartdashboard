package fr.ece.edu.eceproject.gardendashboard.gcm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import fr.ece.edu.eceproject.gardendashboard.app.Config;

/**
 * Created by Pau on 14/02/2016.
 */
public class MyGcmPushReceiver extends GcmListenerService {

    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();
    /**
     * Called when message is received.
     *
     * @param from   SenderID of the sender.
     * @param bundle Data bundle containing message data as key/value pairs.
     *               For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(String from, Bundle bundle) {
        Log.d("GardenDashboard", bundle.toString());
        // app is in foreground, broadcast the push message
        Intent sendNewData = new Intent(Config.RECEIVED_DATA);
        sendNewData.putExtra("data",bundle.getString("message"));
        LocalBroadcastManager.getInstance(this).sendBroadcast(sendNewData);
    }
}
