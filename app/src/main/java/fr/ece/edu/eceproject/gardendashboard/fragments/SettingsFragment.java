package fr.ece.edu.eceproject.gardendashboard.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import fr.ece.edu.eceproject.gardendashboard.R;

/**
 * Created by Pau on 21/02/2016.
 */
public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragment_preference);
    }
}
