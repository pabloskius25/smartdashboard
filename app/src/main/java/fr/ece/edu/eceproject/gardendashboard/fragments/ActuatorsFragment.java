package fr.ece.edu.eceproject.gardendashboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import fr.ece.edu.eceproject.gardendashboard.R;
import fr.ece.edu.eceproject.gardendashboard.model.Actuator;

/**
 * Created by Pau on 27/01/2016.
 */
public class ActuatorsFragment extends ScreenSlidePageFragment {

    private ListView actuatorsList;

    public static ActuatorsFragment newInstance(int index) {
        ActuatorsFragment fragment = new ActuatorsFragment();
        Bundle args = new Bundle();
        args.putInt(INDEX, index);
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.actuators_layout, container, false);

        actuatorsList = (ListView) rootView.findViewById(R.id.actuatorsList);

        return rootView;

    }

    public void updateActuators(ArrayList<Actuator> actuators) {
        ActuatorsArrayAdapter aaa = new ActuatorsArrayAdapter(getContext(),actuators);
        actuatorsList.setAdapter(aaa);
    }
}
