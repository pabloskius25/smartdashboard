package fr.ece.edu.eceproject.gardendashboard.app;

/**
 * Created by Pau on 14/02/2016.
 */
public class Config {

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String RECEIVED_DATA = "receivedData";

}
