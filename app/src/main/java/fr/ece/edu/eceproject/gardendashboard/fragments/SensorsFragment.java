package fr.ece.edu.eceproject.gardendashboard.fragments;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import fr.ece.edu.eceproject.gardendashboard.R;
import fr.ece.edu.eceproject.gardendashboard.model.Sensor;

/**
 * Created by Pau on 27/01/2016.
 */
public class SensorsFragment extends ScreenSlidePageFragment {

    private Switch bluetoothSwitch;
    private Switch serverSwitch;

    private TableLayout tableLayout;

    private HashMap<String,TextView> sensorsMap;

    private ArrayList<fr.ece.edu.eceproject.gardendashboard.model.Sensor> sensors;

    private DecimalFormat format;

    public static SensorsFragment newInstance(int index) {
        SensorsFragment fragment = new SensorsFragment();
        Bundle args = new Bundle();
        args.putInt(INDEX, index);
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.sensors_layout, container, false);

        format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);

        bluetoothSwitch = (Switch) rootView.findViewById(R.id.switch1);
        serverSwitch = (Switch) rootView.findViewById(R.id.switch2);

        tableLayout = (TableLayout) rootView.findViewById(R.id.sensorsTable);

        sensorsMap = new HashMap<>();

        sensors = new ArrayList<>();
        sensors.add(new fr.ece.edu.eceproject.gardendashboard.model.Sensor("Temperature Android",23,"ºC"));
        sensors.add(new fr.ece.edu.eceproject.gardendashboard.model.Sensor("Brightness Android",100,"lx"));
        sensors.add(new fr.ece.edu.eceproject.gardendashboard.model.Sensor("Proximity Android",5.0f,"cm"));
        sensors.add(new fr.ece.edu.eceproject.gardendashboard.model.Sensor("Sound Level Android",5000,"dB"));
        buildTable(sensors);

        return rootView;

    }

    private void buildTable(ArrayList<Sensor> sensors) {
        int rows = sensors.size()/2 + sensors.size()%2;
        for (int i = 0; i < rows; i++) {

            TableRow row = new TableRow(getActivity());
            TableLayout.LayoutParams trlp = new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            trlp.setMargins(0,0,0,50);
            row.setLayoutParams(trlp);

            for (int j = 0; j < 2; j++) {
                if(i*2 + j < sensors.size()) {
                    LinearLayout ll = new LinearLayout(getActivity());
                    ll.setLayoutParams(new TableRow.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    ll.setOrientation(LinearLayout.VERTICAL);
                    ll.setWeightSum(1);

                    TextView name = new TextView(getActivity());
                    name.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
                    //name.setPadding(5, 5, 5, 5);
                    name.setText(sensors.get(i * 2 + j).getName());

                    LinearLayout ll2 = new LinearLayout(getActivity());
                    ll2.setLayoutParams(new TableRow.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
                    ll2.setOrientation(LinearLayout.HORIZONTAL);
                    ll2.setWeightSum(1);

                    TextView value = new TextView(getActivity());
                    value.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
                    //value.setPadding(5, 5, 5, 5);
                    value.setTextSize(55);
                    value.setText(String.valueOf(sensors.get(i * 2 + j).getValue()));

                    TextView units = new TextView(getActivity());
                    units.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT));
                    units.setText(sensors.get(i * 2 + j).getUnits());
                    units.setGravity(Gravity.BOTTOM);
                    units.setTextSize(20);
                    units.setPadding(10,0,0,20);

                    if(j == 1) {
                        ll.setGravity(Gravity.END);
                        ll2.setGravity(Gravity.END);
                        name.setGravity(Gravity.END);
                        value.setGravity(Gravity.END);
                    }

                    sensorsMap.put(sensors.get(i * 2 + j).getName(),value);

                    ll.addView(name);
                    ll2.addView(value);
                    ll2.addView(units);
                    ll.addView(ll2);

                    row.addView(ll);
                }
            }
            tableLayout.addView(row);
        }
        tableLayout.requestLayout();
    }

    public void updateSensorValue(String name, float value) {
        if(sensorsMap != null) {
            TextView tv = sensorsMap.get(name);
            if(tv != null) {
                tv.setText(format.format(value));
            }
        }
    }

    public void addNewSensors(ArrayList<Sensor> newSensors) {
        sensors.addAll(newSensors);
        tableLayout.removeAllViews();
        buildTable(sensors);
    }

    public void updateBluetoothState(boolean value) {
        if(bluetoothSwitch != null) {
            bluetoothSwitch.setChecked(value);
        }
    }

    public void updateServerState(boolean value) {
        if(serverSwitch != null) {
            serverSwitch.setChecked(value);
        }
    }
}
