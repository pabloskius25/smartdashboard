package fr.ece.edu.eceproject.gardendashboard.fragments;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import fr.ece.edu.eceproject.gardendashboard.R;
import fr.ece.edu.eceproject.gardendashboard.model.Actuator;

/**
 * Created by Pau on 09/03/2016.
 */
public class ActuatorsArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final ArrayList<Actuator> actuators;

    public ActuatorsArrayAdapter(Context context, ArrayList<Actuator> actuators) {
        super(context, R.layout.actuator_item);
        this.context = context;
        this.actuators = actuators;
    }

    @Override
    public int getCount() {
        return actuators.size();
    }

    private static class PlaceHolder {

        TextView name;
        Switch value;
        Actuator actuator;

        public static PlaceHolder generate(View convertView) {
            PlaceHolder placeHolder = new PlaceHolder();
            placeHolder.name = (TextView) convertView.findViewById(R.id.actuatorLabel);
            placeHolder.value = (Switch) convertView.findViewById(R.id.actuatorSwitch);
            return placeHolder;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PlaceHolder placeHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.actuator_item, null);
            placeHolder = PlaceHolder.generate(convertView);
            convertView.setTag(placeHolder);
        } else {
            placeHolder = (PlaceHolder) convertView.getTag();
        }

        final Actuator actuator = actuators.get(position);

        placeHolder.actuator = actuator;
        placeHolder.name.setText(actuator.getName());
        placeHolder.value.setChecked(actuator.isActivated());

        return convertView;
    }
}