package fr.ece.edu.eceproject.gardendashboard.activities;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import fr.ece.edu.eceproject.gardendashboard.api.PostTokenAPITask;
import fr.ece.edu.eceproject.gardendashboard.app.Config;
import fr.ece.edu.eceproject.gardendashboard.bluetooth.Bluetooth;
import fr.ece.edu.eceproject.gardendashboard.fragments.ActuatorsFragment;
import fr.ece.edu.eceproject.gardendashboard.fragments.MyFragmentPagerAdapter;
import fr.ece.edu.eceproject.gardendashboard.api.PostValuesAPITask;
import fr.ece.edu.eceproject.gardendashboard.R;
import fr.ece.edu.eceproject.gardendashboard.fragments.SensorsFragment;
import fr.ece.edu.eceproject.gardendashboard.gcm.GcmIntentService;
import fr.ece.edu.eceproject.gardendashboard.model.Actuator;
import fr.ece.edu.eceproject.gardendashboard.model.MyDevice;
import fr.ece.edu.eceproject.gardendashboard.sensors.SoundMeter;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final int REQUEST_ENABLE_BT = 300;
    /**
     * The pager widget, which handles animation and allows swiping horizontally
     * to access previous and next pages.
     */
    private ViewPager pager = null;

    private ActuatorsFragment actuatorsFragment;
    private SensorsFragment sensorsFragment;

    private float proximityValue;
    private float temperatureValue;
    private float lightValue;
    private float soundValue;

    private SoundMeter soundMeter;

    private SensorManager sensorManager;
    private Sensor lightSensor;
    private Sensor proximitySensor;

    private String TAG = MainActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private SharedPreferences prefs;

    private String token;

    private Bluetooth bt;
    private final BluetoothHandler bluetoothHandler = new BluetoothHandler();
    private MyDevice bluetoothDevice;

    private Timer postTimer;

    private long startCronometerInitBluetooth;
    private long startCronometerInfoArduino;
    private long startCronometerSendToken;
    private long startCronometerFetchDataArduino;
    private long startCronometerSendDataServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeBroadcastReceiver();

        initializePreferences();

        initializeContents();

        initializeSensors();

        initializeBluetoothConnections();
    }

    private void initializeBluetoothConnections() {
        bt = new Bluetooth(this, bluetoothHandler);
        startCronometerInitBluetooth = System.currentTimeMillis();
        connectService();

    }

    private void initializePreferences() {
        prefs = getSharedPreferences("fr.ece.edu.eceproject.gardendashboard", MODE_PRIVATE);
        prefs.edit().putString("servAddrPref", "http://eceproject.byte.cat/app_dev.php/api/").apply();
        prefs.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//                if (key.equals("servAddrPref") || key.equals("deviceName"))
//                    sendTokenToServer();
                /*else */
                if (key.equals("postFreqPref") && bluetoothDevice != null) {
                    postTimer.cancel();
                    initializeDataUpdatesTimer();
                }
            }
        });
    }

    public void initializeDataUpdatesTimer() {

        long currentTime = System.currentTimeMillis();
        long difference = currentTime - startCronometerSendToken;
        Log.d(TAG, "Time taken to send token to server (ms): " + difference);

        postTimer = new Timer();
        TimerTask postUpdates = new PostUpdatesAPI();
        postTimer.scheduleAtFixedRate(postUpdates, 0, prefs.getInt("postFreqPref", 5000));
    }

//    public void startRetrievingData() {
//        Log.d(TAG, "Going to send the message: fetch ");
//        bt.sendMessage("fetch");
//    }

    private void initializeSensors() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        soundMeter = new SoundMeter();
        soundMeter.start();

        Timer soundTimer = new Timer();
        TimerTask updateSound = new UpdateSoundLabel();
        soundTimer.scheduleAtFixedRate(updateSound, 0, 100);

        TemperatureReceiver temperatureReceiver = new TemperatureReceiver();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        registerReceiver(temperatureReceiver, localIntentFilter);

        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initializeBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    token = intent.getStringExtra("token");
                    sendTokenToServer();

                    /*if(!prefs.contains("token")) {
                        prefs.edit().putString("token",token).apply();
                    }*/

                } else if (intent.getAction().equals(Config.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL

                    //Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Config.RECEIVED_DATA)) {

                    long currentTime = System.currentTimeMillis();
                    long difference = currentTime - startCronometerSendDataServer;
                    Log.d(TAG, "Time taken to send data to server (ms): " + difference);

                    try {
                        ArrayList<Actuator> actuators = new ArrayList<>();
                        JSONObject resultObj = new JSONObject(intent.getStringExtra("data"));

                        Iterator<String> keys = resultObj.keys();

                        while(keys.hasNext()) {
                            String key = keys.next();
                            actuators.add(new Actuator(key, resultObj.getBoolean(key)));
                        }

                        updateActuators(actuators);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        if (checkPlayServices()) {
            registerGCM();
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.RECEIVED_DATA));
    }

    private void initializeContents() {
        pager = (ViewPager) findViewById(R.id.pager);
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(sensorsFragment = SensorsFragment.newInstance(0));
        adapter.addFragment(actuatorsFragment = ActuatorsFragment.newInstance(1));

        this.pager.setAdapter(adapter);

        final ActionBar actionBar = getSupportActionBar();
        //...

        // Specify that tabs should be displayed in the action bar.
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            @Override
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                // When the tab is selected, switch to the
                // corresponding page in the ViewPager.
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            @Override
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }

        };

        // Add 3 tabs, specifying the tab's text and TabListener
        actionBar.addTab(
                actionBar.newTab()
                        .setText("Sensors")
                        .setTabListener(tabListener));
        actionBar.addTab(
                actionBar.newTab()
                        .setText("Actuators")
                        .setTabListener(tabListener));

        pager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        getSupportActionBar().setSelectedNavigationItem(position);
                    }
                });

    }

    private void sendTokenToServer() {
        if(token != null && bluetoothDevice != null) {
            String apiUrl = prefs.getString("servAddrPref", "http://eceproject.byte.cat/api/"); //change to notDefined
            if (!apiUrl.equals("notDefined")) {
                new PostTokenAPITask(this, token, getDevice(), prefs).execute();
            } else {
                Toast.makeText(getApplicationContext(), "Server address not defined (Settings)", Toast.LENGTH_LONG).show();
            }
        }
    }

    private MyDevice getDevice() {
        String sensors = "Temperature Android:ºC,Brightness Android:lx,Proximity Android:cm,Sound Level Android:dB," +
                bluetoothDevice.getSensors();
        String actuators = bluetoothDevice.getActuators();
        return new MyDevice(sensors,actuators);
    }

    // starting the service to register with GCM
    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            proximityValue = event.values[0];
            sensorsFragment.updateSensorValue("Proximity Android", proximityValue);
        }
        else if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            lightValue = event.values[0];
            sensorsFragment.updateSensorValue("Brightness Android", lightValue);
        }
    }

    public void updateAppActuators(final ArrayList<Actuator> results) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                actuatorsFragment.updateActuators(results);
            }
        });
    }

    public void updateAppActuators(String results) {
        ArrayList<Actuator> actuatorsList = new ArrayList<>();
        String[] actuators = results.split(",");
        for(String act: actuators) {
            actuatorsList.add(new Actuator(act,false));
        }
        updateAppActuators(actuatorsList);
    }

    public void updateAppSensors(String results) {
        ArrayList<fr.ece.edu.eceproject.gardendashboard.model.Sensor> sensorsList = new ArrayList<>();
        String[] sensors = results.split(",");
        for(String sens: sensors) {
            Log.d(TAG, sens);
            String sub[] = sens.split(":");
            sensorsList.add(new fr.ece.edu.eceproject.gardendashboard.model.Sensor(sub[0],0f,sub[1]));
        }
        sensorsFragment.addNewSensors(sensorsList);
    }

    public void updateActuators(final ArrayList<Actuator> results) {
        sendResultsBluetooth(results);
        updateAppActuators(results);
    }

    private void sendResultsBluetooth(ArrayList<Actuator> results) {
        String message = "";
        for(Actuator actuator: results) {
            message += actuator.getName() + ":" + actuator.isActivated() + ";";
        }
        bt.sendMessage("data");
        bt.sendMessage(message);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private class UpdateSoundLabel extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    soundValue = (float) soundMeter.getAmplitude();
                    sensorsFragment.updateSensorValue("Sound Level Android", soundValue);
                }
            });
        }
    }

    private class TemperatureReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            temperatureValue = (float) (intent.getIntExtra("temperature", 0) / 10.0D);
            sensorsFragment.updateSensorValue("Temperature Android", temperatureValue);
        }
    }

    private class PostUpdatesAPI extends TimerTask {

        @Override
        public void run() {
            Log.d(TAG, "Going to send the message: fetch ");
            startCronometerFetchDataArduino = System.currentTimeMillis();
            bt.sendMessage("fetch");
        }
    }

    public void updateServerState(final boolean value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sensorsFragment.updateServerState(value);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private class BluetoothHandler extends Handler {

        private String TAG = BluetoothHandler.class.getSimpleName();

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bluetooth.MESSAGE_STATE_CHANGE:
                    Log.d(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    if(msg.arg1 == 3) {

                        startCronometerInfoArduino = System.currentTimeMillis();
                        long difference = startCronometerInfoArduino - startCronometerInitBluetooth;
                        Log.d(TAG, "Time taken for bluetooth initialization (ms): " + difference);

                        sensorsFragment.updateBluetoothState(true);
                        Log.d(TAG, "Going to send the message: info ");
                        bt.sendMessage("info");
                    }
                    break;
                case Bluetooth.MESSAGE_WRITE:
                    Log.d(TAG, "MESSAGE_WRITE " + msg.obj);
                    //bt.startReading();
                    break;
                case Bluetooth.MESSAGE_READ:
                    Log.d(TAG, "MESSAGE_READ " + msg.obj);
                    //bt.sendMessage("ack\r\n");
                    handleBluetoothDataReceived((String) msg.obj);
                    break;
                case Bluetooth.MESSAGE_DEVICE_NAME:
                    Log.d(TAG, "MESSAGE_DEVICE_NAME " + msg);
                    break;
                case Bluetooth.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST "+msg);
                    connectService();
                    break;
            }
        }

    }

    public void connectService(){
        try {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter.isEnabled()) {
                bt.start();
                bt.connectDevice("HC-06");
                bt.connectDevice("30:14:11:27:03:09");
                Log.d(TAG, "Btservice started - listening");
            } else {
                Log.w(TAG, "Btservice started - bluetooth is not enabled");
            }
        } catch(Exception e){
            Log.e(TAG, "Unable to start bt ",e);
        }
    }

    private void handleBluetoothDataReceived(String data) {
        data = data.replaceAll("\n","");
        if(data.startsWith("info")) {

            startCronometerSendToken = System.currentTimeMillis();
            long difference = startCronometerSendToken - startCronometerInfoArduino;
            Log.d(TAG, "Time taken to fetch info Arduino (ms): " + difference);

            data = data.substring(4);
            String[] substr = data.split(";");
            bluetoothDevice = new MyDevice(substr[0],substr[1]);
            updateAppSensors(substr[0]);
            updateAppActuators(substr[1]);
            sendTokenToServer();
        }
        else if(data.startsWith("data")) {

            long currentTime = System.currentTimeMillis();
            long difference = currentTime - startCronometerFetchDataArduino;
            Log.d(TAG, "Time taken to fetch data Arduino (ms): " + difference);

            Log.d(TAG, "Data received: " + data);
            data = data.substring(4);
            String[] substr = data.split(";");
            JSONObject json = getJSONAndroidData();
            try {
                for(String value: substr) {
                    Log.d(TAG, "Substring extracted: " + value);
                    String[] parts = value.split(":");
                    if(!parts[0].equals("interruptor")) {
                        json.put(parts[0], Integer.parseInt(parts[1]));
                        sensorsFragment.updateSensorValue(parts[0], Float.parseFloat(parts[1]));
                    }
                    else
                        json.put(parts[0], parts[1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Posting values to server: " + json.toString());
            startCronometerSendDataServer = System.currentTimeMillis();
            new PostValuesAPITask(prefs,json).execute();
        }
    }

    private JSONObject getJSONAndroidData(){
        JSONObject json = new JSONObject();
        try {
            json.put("Temperature Android",temperatureValue);
            json.put("Brightness Android",lightValue);
            json.put("Proximity Android",proximityValue);
            json.put("Sound Level Android",soundValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

}