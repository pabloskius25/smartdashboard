package fr.ece.edu.eceproject.gardendashboard.api;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import fr.ece.edu.eceproject.gardendashboard.activities.MainActivity;

/**
 * Created by Pau on 02/02/2016.
 */
public class PostValuesAPITask extends AsyncTask <Void,Void,Void> {

    private ContentValues parameters;
    private SharedPreferences prefs;
    private String TAG = PostValuesAPITask.class.getSimpleName();

    public PostValuesAPITask(SharedPreferences prefs, JSONObject json) {
        this.prefs = prefs;
        parameters = new ContentValues();
        parameters.put("data",json.toString());

        Log.d(TAG, "JSON to send: " + json.toString());
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;

        try {
            String device = prefs.getString("idDevice", "-1");
            URL url = new URL(prefs.getString("servAddrPref","") + "devices/" + device);

            Log.d(TAG, "Sending data to server as device " + device);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("PUT");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.connect();

            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(parameters.toString());
            out.close();

            Log.d(TAG, String.valueOf(urlConnection.getResponseCode()));

//            String f = "";
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
//            String line;
//            while((line = in.readLine()) != null) {
//                f += line;
//            }
//            in.close();
//
//            Log.d("Error Response: ", f);

        } catch (IOException e) {
            Log.e("gardenDashboard", "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }
}
